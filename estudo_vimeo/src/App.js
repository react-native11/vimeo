import React from 'react';

import  {SignedOutRoutes}   from './routes/routes'
export default class App extends React.Component{
  
  render(){
    return <SignedOutRoutes/>;
  }
};
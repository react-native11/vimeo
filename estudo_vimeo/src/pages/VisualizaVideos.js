import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import Video from 'react-native-video';
import commonStyles from '../common/commonStyles';


export default class VisualizarVideos extends React.Component{
  
  constructor(props){
    super(props)
    const url = this.props.navigation.getParam('url')
    this.state =  {url}
}


  render(){
    return (
      <View style={styles.video}>
        <Video  source={{uri: this.state.url}}   // Can be a URL or a local file.
                ref={(ref) => {   this.player = ref  }} 
                style={styles.backgroundVideo}
                onBuffer={this.onBuffer}                // Callback when remote video is buffering
                onError={this.videoError}               // Callback when video cannot be loaded
                style={styles.backgroundVideo}
                resizeMode="contain"
                fullscreenOrientation="all"
                fullscreen={true}
                controls={true}/>
      </View>
    );

  }
};

const styles = StyleSheet.create({
  container: {
      flex:1
  },
  addButton: {
      position: 'absolute',
      right:30,
      bottom:30,
      width:50,
      height:50,
      backgroundColor:commonStyles.colors.today,
      borderRadius:25,
      justifyContent:'center',
      alignItems:'center'
  },video:{
    flex: 1, 
    backgroundColor:'#000000',
    alignItems:'stretch'

},backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
})
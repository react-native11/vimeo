import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import Video from 'react-native-video';

export default class Backup extends React.Component{
  
  render(){
    return (
      <View style={styles.video}>
        <Video  source={{uri: "https://vod-progressive.akamaized.net/exp=1593578746~acl=%2Fvimeo-prod-skyfire-std-us%2F01%2F1722%2F17%2F433611514%2F1887686169.mp4~hmac=d02b0799562546043a4711dc0470852bee15fed086558670898cad4305304d4a/vimeo-prod-skyfire-std-us/01/1722/17/433611514/1887686169.mp4"}}   // Can be a URL or a local file.
                ref={(ref) => {   this.player = ref  }} 
              style={styles.backgroundVideo}
              
              onBuffer={this.onBuffer}                // Callback when remote video is buffering
              onError={this.videoError}               // Callback when video cannot be loaded
              style={styles.backgroundVideo}
              resizeMode="contain"
              fullscreenOrientation="all"
              fullscreen={true}
              controls={true}/>
      </View>
    );

  }
};

// Later on in your styles..
var styles = StyleSheet.create({
  video:{
    flex: 1, 
    backgroundColor:'#000000',
    alignItems:'stretch'

},backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

import React from 'react';
import { View, TouchableOpacity,StyleSheet, Text,TouchableHighlight} from 'react-native';
import commonStyles from '../common/commonStyles';
import { apiVideos, apiPlayer } from '../services/api';

export default class Videos extends React.Component{
  
  componentDidMount(){
    this.setState({ videos: []});
    this.carregarVideos()
  }

  carregarVideos = async () => {
    //console.log('DropBox List Folder')
    const config = { headers: {'Authorization': `Bearer c0c334ecea6fbb9bd23fb30e51a2e1a2`} };
    await apiVideos.get('118481091/videos/',config)
      .then((response) => {
        this.setState({ videos: response.data })
        
        console.log(response.data)
        console.log(response.data.data)

        console.log(response.data.data[0].name)
        console.log(response.data.data[0].resource_key)
        
        console.log(response.data.data[1])
        
      })
      .catch(error => { console.log(error);});
  };

  getTemporaryLink = async (video) => {
    console.log('Gerando link temporario')
    const config = { headers: {'Authorization': `Bearer c0c334ecea6fbb9bd23fb30e51a2e1a2`} };
    let url ='';

    let uri = video.uri.split('/')
    console.log(uri)

    await apiPlayer.get(`/${uri[2]}/config`,config)
     .then((response) => {
        console.log('Link temporario gerado!');
        console.log("data",JSON.stringify(response.data.request.files.progressive[0].url, null, 4)); 
          
        this.submitVideo(response.data.request.files.progressive[0].url);
     })
     .catch(error => { console.log(error);});
  }

  submitVideo(url){
    if(url){
      this.props.navigation.navigate('VisualizaVideos', {url})

    }
  }

  render(){
    return (
      <View style={styles.container}>
        <Text>Procurando o Button</Text>

        { 
          this.state && this.state.videos && this.state.videos.data ? 
          (
            this.state.videos.data.map((video, index)=>(
              <View key={video.resource_key}>
                <Text>{video.resource_key}</Text>
                <Text>{video.name}</Text>

                <TouchableHighlight style={styles.button} onPress={() =>this.getTemporaryLink(video)}>
                    <Text>
                        Play
                    </Text>
                </TouchableHighlight>

              </View>
            ))
          )     
          
          :<Text>Lista Vazia</Text>
        
      
        }

        <TouchableOpacity style={styles.addButton}
            activeOpacity={0.7}
            onPress={() => this.props.navigation.navigate('VisualizaVideos')}>
            <Text>Teste</Text>
        </TouchableOpacity>

      </View>
    );

  }
};

const styles = StyleSheet.create({
  container: {
      flex:1
  },
  addButton: {
      position: 'absolute',
      right:30,
      bottom:30,
      width:50,
      height:50,
      backgroundColor:commonStyles.colors.today,
      borderRadius:25,
      justifyContent:'center',
      alignItems:'center'
  },
  button: {
    width:50,
    height:50,
    backgroundColor:commonStyles.colors.today,
    borderRadius:25,
    justifyContent:'center',
    alignItems:'center'
  },
})
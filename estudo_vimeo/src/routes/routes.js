import React, { Component } from "react";
import {createAppContainer,createSwitchNavigator} from 'react-navigation'
import Videos from '../pages/Videos'
import Backup from '../pages/Backup'
import VisualizaVideos from '../pages/VisualizaVideos'

export const SignedOutRoutes = createAppContainer( createSwitchNavigator({
    Videos, 
    Backup,
    VisualizaVideos
    
  }));
